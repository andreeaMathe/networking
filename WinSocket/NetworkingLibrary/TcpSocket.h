#pragma once

#include <iostream>
#include <string>
#include <winsock2.h>
#include <ws2tcpip.h>

class TcpSocket
{
private:
	static const SOCKET InvalidSocketHandle;

public:
	TcpSocket();
	~TcpSocket();

	void Listen(uint16_t port);
	TcpSocket Accept();
	void Connect(const std::string& remoteAddress, uint16_t port);

	void Send(const void* data, uint32_t size);
	void Receive(void* data, uint32_t size, int& recieved);

private:
	TcpSocket(SOCKET socket);

private:
	SOCKET m_socket;
};

