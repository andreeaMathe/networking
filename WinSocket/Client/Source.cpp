#include<iostream>
#include<array>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses
#include "../NetworkingLibrary/TcpSocket.h"

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

int main(int argc, char* argv[]) 
{
	TcpSocket connectSocket;
	connectSocket.Connect("localhost", 27015);

	std::string message = "this is a test";
	std::cout << "Sending message: " << message << std::endl;
	connectSocket.Send(message.c_str(), message.size());


	std::array<char, 512> receiveBuffer;
	int received;
	connectSocket.Receive(receiveBuffer.data(), receiveBuffer.size(), received);
	std::cout << "Received: ";
	std::copy(receiveBuffer.begin(), receiveBuffer.begin() + received, std::ostream_iterator<char>(std::cout, ""));
	std::cout << std::endl;

	return 0;
}